public class Quiz3_1{
	static Beverage drink = new Tea();
	
	public static void main(String[] args){
		System.out.println(drink.getCost());
		drink = new Sugar(drink);
		System.out.println(drink.getCost());
	}
}

interface Beverage{
	double getCost();
}

class Tea implements Beverage{
	private final double price = 2.0;
	
	public Tea(Beverage beverage){
		this.wrappedBeverage = beverage;
	}
	
	public Tea(){
		this(null);
	}
	
	@Override
	public double getCost(){
		return price;
	}
}

class Coffee implements Beverage{
	private final double price = 3.0;
	
	public Coffee(Beverage beverage){
		this.wrappedBeverage = beverage;
	}
	
	public Coffee(){
		this(null);
	}
	
	@Override
	public double getCost(){
		return price;
	}
}

class Sugar implements Beverage{
	private final double price = 3.5;
	private Beverage wrappedBeverage;
	
	public Sugar(Beverage beverage){
		this.wrappedBeverage = beverage;
	}
	
	public Sugar(){
		this(null);
	}
	
	@Override
	public double getCost(){
		if(wrappedBeverage == null) return price;
		return price + wrappedBeverage.getCost();
	}
}

class Milk implements Beverage{
	private final double price = 2.5;
	private Beverage wrappedBeverage;
	
	public Milk(Beverage beverage){
		this.wrappedBeverage = beverage;
	}
	
	public Milk(){
		this(null);
	}
	
	@Override
	public double getCost(){
		if(wrappedBeverage == null) return price;
		return price + wrappedBeverage.getCost();
	}
}