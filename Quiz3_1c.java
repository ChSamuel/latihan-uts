public class Quiz3_1c{
	static MachineState state = new StartingCondition();
	
	public static void main(String[] args){
		state.seeStatus();
		state.brewBeverage();
		state.swipeCreditCard();
		state.brewBeverage();
		state.swipeCreditCard();
		state.addingCondiments();
		state.pouring();
		state.addingCondiments();
	}
}

interface MachineState{
	void swipeCreditCard();
	void brewBeverage();
	void addingCondiments();
	void pouring();
	void seeStatus();
}

class StartingCondition implements MachineState{
	public void swipeCreditCard(){
		Quiz3_1c.state = new BrewingCondition();
		Quiz3_1c.state.seeStatus();
	}
	
	public void brewBeverage(){
		System.out.println("Invalid command");
	}
	
	public void addingCondiments(){
		System.out.println("Invalid command");
	}
	
	public void pouring(){
		System.out.println("Invalid command");
	}
	
	public void seeStatus(){
		System.out.println("Machine in Starting Condition");
	}
}

class BrewingCondition implements MachineState{
	public void swipeCreditCard(){
		System.out.println("Invalid command");
	}
	
	public void brewBeverage(){
		Quiz3_1c.state = new MixingCondition();
		Quiz3_1c.state.seeStatus();
	}
	
	public void addingCondiments(){
		System.out.println("Invalid command");
	}
	
	public void pouring(){
		System.out.println("Invalid command");
	}
	
	public void seeStatus(){
		System.out.println("Machine in Brewing Condition");
	}
}

class MixingCondition implements MachineState{
	public void swipeCreditCard(){
		System.out.println("Invalid command");
	}
	
	public void brewBeverage(){
		System.out.println("Invalid command");
	}
	
	public void addingCondiments(){
		Quiz3_1c.state = new PouringCondition();
		Quiz3_1c.state.seeStatus();
	}
	
	public void pouring(){
		System.out.println("Invalid command");
	}
	
	public void seeStatus(){
		System.out.println("Machine in Mixing Condition");
	}
}

class PouringCondition implements MachineState{
	public void swipeCreditCard(){
		System.out.println("Invalid command");
	}
	
	public void brewBeverage(){
		System.out.println("Invalid command");
	}
	
	public void addingCondiments(){
		System.out.println("Invalid command");
	}
	
	public void pouring(){
		Quiz3_1c.state = new StartingCondition();
		Quiz3_1c.state.seeStatus();
	}
	
	public void seeStatus(){
		System.out.println("Machine in Pouring Condition");
	}
}